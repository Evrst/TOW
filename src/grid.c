#include<stdlib.h>
#include <SDL2/SDL.h>
#include "func.h"

int createGrid(int grid[20][10]){
    for (int i=0;i<20;i++){
        for (int j=0;j<10;j++){
            grid[i][j]=0;
        }
    }
}

int gridAdd(int piece[2][4],int cursor[2],int angle,int grid[20][10],int type){
    for (int i=0;i<2;i++){
        for (int j=0;j<4;j++) {
            if(piece[i][j]){    
                if(type==1||type==2||type==6){
                    switch(angle){
                        case 0:
                        grid[i+cursor[0]][j+cursor[1]]=piece[i][j];
                        break;    
                        case 1:
                        grid[-j+cursor[0]+2][i+cursor[1]+2]=piece[i][j];
                        break;    
                        case 2:
                        grid[-i+cursor[0]][-j+cursor[1]+4]=piece[i][j];
                        break;    
                        case 3:
                        grid[j+cursor[0]-2][-i+cursor[1]+2]=piece[i][j];
                        break;    
                    }    
                }
                else if(type==4){
                    grid[i+cursor[0]][j+cursor[1]]=piece[i][j];
                }    
                else if(type==7){
                    if(!(angle%2))
                        grid[i+cursor[0]][j+cursor[1]]=piece[i][j];
                    else
                        grid[-j+cursor[0]+1][i+cursor[1]+2]=piece[i][j];
                }    
                else{
                    if(!(angle%2))
                        grid[i+cursor[0]][j+cursor[1]]=piece[i][j];
                    else
                        grid[-j+cursor[0]+2][i+cursor[1]+2]=piece[i][j];
                }    
            }    
        }
    }
}

int line(int grid[20][10],int *lineSum){
    int nline=0;
    for(int i=19;i>-1;i--){
        int full=1;
        for(int j=0;j<10;j++){
            if(!grid[i][j]){
                full=0;
                break;
            }    
        }
        if(full){
            nline++;
            for(int j=i-1;j>0;j--){
                for(int k=0;k<10;k++)
                    grid[j+1][k]=grid[j][k];
            }
            for(int j=0;j<10;j++)
                grid[0][j]=0;
            i++;
        }    
    }
    *lineSum+=nline;
    return scoreFromLine(nline);
}

