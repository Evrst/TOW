#include<stdlib.h>
#include <SDL2/SDL.h>
#include "func.h"

int printGrid(int grid[20][10],SDL_Renderer* renderer,int level){

    SDL_Rect square;  
    square.w = 30;
    square.h = 30;

    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);

    for (int i=0;i<20;i++){
        for (int j=0;j<10;j++){
            switch(grid[i][j]){
                case 0:
                    SDL_SetRenderDrawColor(renderer,255,255,255,255);
                    break;
                case 1:
                    SDL_SetRenderDrawColor(renderer,(248+10*(level-1))%255+10, 192, 0,255);
                    break;
                case 2:
                    SDL_SetRenderDrawColor(renderer,(6+10*(level-1))%255+10, 152, 0,255);
                    break;
                case 3:
                    SDL_SetRenderDrawColor(renderer,(6+10*(level-1))%255+10, 152, 0,255);
                    break;
                case 4:
                    SDL_SetRenderDrawColor(renderer,(248+10*(level-1))%255+10, 192, 0,255);
                    break;
                case 5:
                    SDL_SetRenderDrawColor(renderer,(10*(level-1))%255+10, 105, 255,255);
                    break;
                case 6:
                    SDL_SetRenderDrawColor(renderer,(10*(level-1))%255+10, 105, 255,255);
                    break;
                case 7:
                    SDL_SetRenderDrawColor(renderer,(200+10*(level-1))%255+10, 6, 0,255);
                    break;
            }
            square.x=30*j;
            square.y=30*i;
            SDL_RenderFillRect(renderer, &square);
        }
    }
}
int printPiece(int piece[2][4],int cursor[2],int angle,SDL_Renderer* renderer,int type,int level){
    SDL_Rect square;  
    square.w = 30;
    square.h = 30;
    for (int i=0;i<2;i++){
        for (int j=0;j<4;j++){
            switch(piece[i][j]){
                case 1:
                SDL_SetRenderDrawColor(renderer,(248+10*(level-1))%255+10, 192, 0,255);
                break;
                case 2:
                SDL_SetRenderDrawColor(renderer,(6+10*(level-1))%255+10, 152, 0,255);
                break;
                case 3:
                SDL_SetRenderDrawColor(renderer,(6+10*(level-1))%255+10, 152, 0,255);
                break;
                case 4:
                SDL_SetRenderDrawColor(renderer,(248+10*(level-1))%255+10, 192, 0,255);
                break;
                case 5:
                SDL_SetRenderDrawColor(renderer,(0+10*(level-1))%255+10, 105, 255,255);
                break;
                case 6:
                SDL_SetRenderDrawColor(renderer,(0+10*(level-1))%255+10, 105, 255,255);
                break;
                case 7:
                SDL_SetRenderDrawColor(renderer,(200+10*(level-1))%255+10, 6, 0,255);
                break;
            }
            if(type==1||type==2||type==6){
                switch(angle){
                    case 0:
                    square.x=30*(j+cursor[1]),square.y=30*(i+cursor[0]);
                    break;    
                    case 1:
                    square.x=30*(i+cursor[1]+2),square.y=30*(-j+cursor[0]+2);
                    break;    
                    case 2:
                    square.x=30*(-j+cursor[1]+4),square.y=30*(-i+cursor[0]);
                    break;    
                    case 3:
                    square.x=30*(-i+cursor[1]+2),square.y=30*(j+cursor[0]-2);
                    break;    
                }    
            }
            else if(type==4){
                    square.x=30*(j+cursor[1]),square.y=30*(i+cursor[0]);
            }    
            else if(type==7){
                if(!(angle%2))
                    square.x=30*(j+cursor[1]),square.y=30*(i+cursor[0]);
                else
                    square.y=30*(-j+cursor[0]+1),square.x=30*(i+cursor[1]+2);
            }    
            else{
                if(!(angle%2))
                    square.x=30*(j+cursor[1]),square.y=30*(i+cursor[0]);
                else
                    square.y=30*(-j+cursor[0]+2),square.x=30*(i+cursor[1]+2);
            }    
            if(piece[i][j])
                SDL_RenderFillRect(renderer, &square);
        }
    }    
}


