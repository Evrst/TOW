static inline void setPiece(int piece[2][4],int value0,int value1,int value2,int value3,int value4,int value5,int value6,int value7){
    piece[0][0]=value0;
    piece[0][1]=value1;
    piece[0][2]=value2;
    piece[0][3]=value3;
    piece[1][0]=value4;
    piece[1][1]=value5;
    piece[1][2]=value6;
    piece[1][3]=value7;
}    

void createPiece(int piece[2][4],int type){
    switch (type){
        case 1:
            setPiece(piece,0,1,1,1,0,0,1,0);
            break;
        case 2:
            setPiece(piece,0,2,2,2,0,0,0,2);
            break;
        case 3:
            setPiece(piece,0,3,3,0,0,0,3,3);
            break;
        case 4:
            setPiece(piece,0,4,4,0,0,4,4,0);
            break;
        case 5:
            setPiece(piece,0,0,5,5,0,5,5,0);
            break;
        case 6:
            setPiece(piece,0,6,6,6,0,6,0,0);
            break;
        case 7:
            setPiece(piece,7,7,7,7,0,0,0,0);
            break;
    }        
    /*for(int i=0;i<2;i++){
        for(int j=0;j<4;j++){
            printf("%d",piece[i][j]);
        }
        printf("\n");
    }*/
}
