#include "func.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "config.h"

int main(){
    srand(clock());
    struct timeval t1, t2;
    gettimeofday(&t1, NULL);
    int score=0;
    int lineSum=0;
    int angle=0;
    int movingState=0;
    float gravity=48;
    const int gravityValue[10]={48,43,38,33,28,23,18,13,8,6};
    int level=0;
    int countFps=0;
    double timesec=0;
    unsigned long time0; /* temporary value for testing frame change */
    unsigned long time= (unsigned long) ( timesec * FREQUENCY )%TODOMAXLONG;  /* frame number for computing events */
    int cursor[2]={0,3};
    int nextPieceCursor[2]={10,12};
    int running = 1;
    int gotEvent;
    int grid[20][10];
    createGrid(grid);
    int lastPiece=0;
    int nextPiece=0;
    lastPiece=choosePiece(lastPiece);
    nextPiece=choosePiece(lastPiece);
    int piece[2][4];
    int nextPieceModel[2][4];
    createPiece(piece,lastPiece);
    createPiece(nextPieceModel,nextPiece);
    SDL_Init(SDL_INIT_VIDEO);
    if(TTF_Init()==-1)
    {
      printf("TTF_Init: %s\n", TTF_GetError());
      exit(2);
    }
    atexit(SDL_Quit);
    SDL_Window* window = SDL_CreateWindow("image_test",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,500,600,SDL_WINDOW_RESIZABLE);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC); // Création du renderer
    if(renderer == NULL){
        fprintf(stderr,"Erreur lors de la creation d'un renderer : %s",SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Texture* SCORE;
    SDL_Texture* LEVEL;
    SDL_Surface* textSurface;
    SDL_Rect SCORERect;
    SDL_Rect LEVELRect;
    SDL_Color blanc={250,250,250};
    TTF_Font *textFont = NULL;
    textFont = TTF_OpenFont("../fonts/8bit.otf", 27);
    char textString[50];
    sprintf(textString,"score");
    SCORERect.x = 310;
    SCORERect.y = 30;
    LEVELRect.x = 310;
    LEVELRect.y = 130;
    sprintf(textString,"score");
    textSurface = TTF_RenderText_Solid(textFont,textString,blanc);
    SCORE = SDL_CreateTextureFromSurface(renderer,textSurface);
    sprintf(textString,"level");
    textSurface = TTF_RenderText_Solid(textFont,textString,blanc);
    LEVEL = SDL_CreateTextureFromSurface(renderer,textSurface);
    SDL_QueryTexture(SCORE, NULL, NULL, &SCORERect.w, &SCORERect.h);
    SDL_QueryTexture(LEVEL, NULL, NULL, &LEVELRect.w, &LEVELRect.h);
    SDL_Texture* scoreText;
    SDL_Surface* scoreSurface;
    SDL_Rect scoreRect;
    SDL_Texture* levelText;
    SDL_Surface* levelSurface;
    SDL_Rect levelRect;

    SDL_Event Event;
    while (running){
        //fprintf(stdout,"%f %ld\n",timesec,time0);
        gettimeofday(&t2, NULL);
        timesec = (t2.tv_sec - t1.tv_sec) + ((t2.tv_usec - t1.tv_usec) * 1e-6);
        time0 = (unsigned long) ( timesec * FREQUENCY )%TODOMAXLONG; /* frame number */
        if (time0 > time||time0==0) {
            time = time0; /* new frame is activated */
            countFps =(countFps+1)%2048;
            level=lineSum/10;
            printGrid(grid,renderer,level);
            printPiece(piece,cursor,angle,renderer,lastPiece,level);
            printScore(score,renderer,SCORE,SCORERect,scoreText,&scoreSurface,scoreRect,textFont,blanc);
            printLevel(level,renderer,LEVEL,LEVELRect,levelText,&levelSurface,levelRect,textFont,blanc);
            printPiece(nextPieceModel,nextPieceCursor,0,renderer,nextPiece,level);
            if(fmod(countFps,gravity)==0){
                if(!downColliding(piece,angle,cursor,grid,lastPiece))
                    cursor[0]++;
                else if(!isColliding(piece,angle,cursor,grid,lastPiece)){
                    gridAdd(piece,cursor,angle,grid,lastPiece);
                    gravity=gravityValue[level];
                    score+=line(grid,&lineSum);
                    angle=0;
                    lastPiece=nextPiece;
                    createPiece(piece,lastPiece);
                    nextPiece=choosePiece(nextPiece);
                    createPiece(nextPieceModel,nextPiece);
                    cursor[0]=0,cursor[1]=3;
                }
                else
                    running=0;
            }
            SDL_RenderPresent(renderer);
        }
        gotEvent = SDL_PollEvent(&Event);
        while (gotEvent){
            input(Event,&running);
            gotEvent = SDL_PollEvent(&Event);
        }
        shift(cursor,piece,lastPiece,angle,grid,countFps,&gravity,gravityValue[level],&movingState,&score);
        rotate(piece,lastPiece,&angle,grid,cursor);
    }
    SDL_FreeSurface(textSurface); //Équivalent du destroyTexture pour les surface, permet de libérer la mémoire quand on n'a plus besoin d'une surface
    SDL_DestroyTexture(SCORE);
    SDL_FreeSurface(scoreSurface); //Équivalent du destroyTexture pour les surface, permet de libérer la mémoire quand on n'a plus besoin d'une surface
    SDL_DestroyTexture(scoreText);
    SDL_DestroyTexture(LEVEL);
    SDL_FreeSurface(levelSurface); //Équivalent du destroyTexture pour les surface, permet de libérer la mémoire quand on n'a plus besoin d'une surface
    SDL_DestroyTexture(levelText);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}
