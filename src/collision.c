int leftColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type){
    int coll=0;
    for(int i=0;i<2&&!coll;i++){
        for(int j=0;j<4&&!coll;j++){
            if (piece[i][j]){
                switch(angle){
                    case 0:
                    if(+j-1+cursor[1]<0)
                        coll=1;
                    else if(i+cursor[0]>-1&&i+cursor[0]<20){
                        if(grid[+i+cursor[0]][+j-1+cursor[1]])
                            coll=1;
                    }
                    break;
                    case 1:
                    if(type==1||type==2||type==6){
                        if(+i+cursor[1]+1<0)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+1])
                                coll=1;
                        }
                    }
                    else if(type==4){
                        if(+j+cursor[1]-1<0)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]-1])
                                coll=1;
                        }
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+1<0)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&-j+cursor[0]+1<20){
                            if(grid[-j+cursor[0]+1][+i+cursor[1]+1])
                                coll=1;
                        }
                    }    
                    else{
                        if(+i+cursor[1]+1<0)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+1])
                                coll=1;
                        }
                    }
                    break;
                    case 2:
                    if(type==1||type==2||type==6){
                        if(-j+cursor[1]+3<0)
                            coll=1;
                        else if(-i+cursor[0]>-1&&-i+cursor[0]<20){
                            if(grid[-i+cursor[0]][-j+cursor[1]+3])
                                coll=1;
                        }
                    }
                    else if(type==4){
                        if(+j+cursor[1]-1<0)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]-1])
                                coll=1;
                        }
                    }    
                    else if(type==7){
                        if(+j+cursor[1]-1<0)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]-1])
                                coll=1;
                        }
                    }    
                    else{
                        if(+j+cursor[1]-1<0)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]-1])
                                coll=1;
                        }
                    }    
                    break;
                    case 3:
                    if(type==1||type==2||type==6){
                        if(-i+cursor[1]+1<0)
                            coll=1;
                        else if(j+cursor[0]-2>-1&&j+cursor[0]-2<20){
                            if(grid[+j+cursor[0]-2][-i+cursor[1]+1])
                                coll=1;
                        }
                    }
                    else if(type==4){
                        if(+j+cursor[1]-1<0)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]-1])
                                coll=1;
                        }
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+1<0)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&-j+cursor[0]+1<20){
                            if(grid[-j+cursor[0]+1][+i+cursor[1]+1])
                                coll=1;
                        }
                    }    
                    else{
                        if(+i+cursor[1]+1<0)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+1])
                                coll=1;
                        }
                    }    
                    break;
                    default:
                    break;
                }    
            }
        }
    }
    return coll;
}
int rightColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type){
    int coll=0;
    for(int i=0;i<2&&!coll;i++){
        for(int j=0;j<4&&!coll;j++){
            if (piece[i][j]){
                switch(angle){
                    case 0:
                    if(+j+1+cursor[1]>9)
                        coll=1;
                    else if(i+cursor[0]>-1&&i+cursor[0]<20){
                        if(grid[+i+cursor[0]][+j+1+cursor[1]])
                            coll=1;
                    }        
                    break;
                    case 1:
                    if(type==1||type==2||type==6){
                        if(+i+cursor[1]+3>9)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+3])
                                coll=1;
                        }        
                    }
                    else if(type==4){
                        if(+j+cursor[1]+1>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]+1])
                                coll=1;
                        }        
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+3>9)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&-j+cursor[0]+1<20){
                            if(grid[-j+cursor[0]+1][+i+cursor[1]+3])
                                coll=1;
                        }        
                    }    
                    else{
                        if(+i+cursor[1]+3>9)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+3])
                                coll=1;
                        }        
                    }
                    break;
                    case 2:
                    if(type==1||type==2||type==6){
                        if(-j+cursor[1]+5>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][-j+cursor[1]+5])
                                coll=1;
                        }        
                    }
                    else if(type==4){
                        if(+j+cursor[1]+1>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]+1])
                                coll=1;
                        }        
                    }    
                    else if(type==7){
                        if(+j+cursor[1]+1>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]+1])
                                coll=1;
                        }        
                    }    
                    else{
                        if(+j+cursor[1]+1>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]+1])
                                coll=1;
                        }        
                    }    
                    break;
                    case 3:
                    if(type==1||type==2||type==6){
                        if(-i+cursor[1]+3>9)
                            coll=1;
                        else if(j+cursor[0]-2>-1&&j+cursor[0]-2<20){
                            if(grid[+j+cursor[0]-2][-i+cursor[1]+3])
                                coll=1;
                        }        
                    }
                    else if(type==4){
                        if(+j+cursor[1]+1>9)
                            coll=1;
                        else if(i+cursor[0]>-1&&i+cursor[0]<20){
                            if(grid[+i+cursor[0]][+j+cursor[1]+1])
                                coll=1;
                        }        
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+3>9)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&-j+cursor[0]+1<20){
                            if(grid[-j+cursor[0]+1][+i+cursor[1]+3])
                                coll=1;
                        }        
                    }    
                    else{
                        if(+i+cursor[1]+3>9)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&-j+cursor[0]+2<20){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+3])
                                coll=1;
                        }        
                    }    
                    break;
                    default:
                    break;
                }    
            }
        }
    }
    return coll;
}
int downColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type){
    int coll=0;
    for(int i=0;i<2&&!coll;i++){
        for(int j=0;j<4&&!coll;j++){
            if (piece[i][j]){
                switch(angle){
                    case 0:
                    if(+i+1+cursor[0]>19)
                        coll=1;
                    else if(+i+1+cursor[0]>-1){
                        if(grid[+i+1+cursor[0]][+j+cursor[1]])
                            coll=1;
                    }    
                    break;
                    case 1:
                    if(type==1||type==2||type==6){
                        if(-j+cursor[0]+3>19)
                            coll=1;
                        else if(-j+cursor[0]+3>-1){
                            if(grid[-j+cursor[0]+3][+i+cursor[1]+2])
                                coll=1;
                        }
                    }    
                    else if(type==4){
                        if(+i+cursor[0]+1>19)
                            coll=1;
                        else if(+i+cursor[0]+1>-1){
                            if(grid[+i+cursor[0]+1][+j+cursor[1]])
                                coll=1;
                        }    
                    }    
                    else if(type==7){
                        if(-j+cursor[0]+2>19)
                            coll=1;
                        else if(-j+cursor[0]+2>-1){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+2])
                                coll=1;
                        }    
                    }    
                    else{
                        if(-j+cursor[0]+3>19)
                            coll=1;
                        else if(-j+cursor[0]+3>-1){
                            if(grid[-j+cursor[0]+3][+i+cursor[1]+2])
                                coll=1;
                        }
                    }    
                    break;
                    case 2:
                    if(type==1||type==2||type==6){
                        if(-i+cursor[0]+1>19)
                            coll=1;
                        else if(-i+cursor[0]+1>-1){
                            if(grid[-i+cursor[0]+1][-j+cursor[1]+4])
                                coll=1;
                        }
                    }    
                    else if(type==4){
                        if(+i+cursor[0]+1>19)
                            coll=1;
                        else if(+i+cursor[0]+1>-1){
                            if(grid[+i+cursor[0]+1][+j+cursor[1]])
                                coll=1;
                        }    
                    }    
                    else if(type==7){
                        if(+i+cursor[0]+1>19)
                            coll=1;
                        else if(+i+cursor[0]+1>-1){
                            if(grid[+i+cursor[0]+1][+j+cursor[1]])
                                coll=1;
                        }    
                    }    
                    else{
                        if(+i+cursor[0]+1>19)
                            coll=1;
                        else if(+i+cursor[0]+1>-1){
                            if(grid[+i+cursor[0]+1][+j+cursor[1]])
                                coll=1;
                        }    
                    }    
                    break;
                    case 3:
                    if(type==1||type==2||type==6){
                        if(+j+cursor[0]-1>19)
                            coll=1;
                        else if(+j+cursor[0]-1>-1){
                            if(grid[+j+cursor[0]-1][-i+cursor[1]+2])
                                coll=1;
                        }
                    }    
                    else if(type==4){
                        if(+i+cursor[0]+1>19)
                            coll=1;
                        else if(+i+cursor[0]+1>-1){
                            if(grid[+i+cursor[0]+1][+j+cursor[1]])
                                coll=1;
                        }    
                    }    
                    else if(type==7){
                        if(+i+cursor[0]+2>19)
                            coll=1;
                        else if(-j+cursor[0]+2>-1){
                            if(grid[-j+cursor[0]+2][+i+cursor[1]+2])
                                coll=1;
                        }    
                    }    
                    else{
                        if(-j+cursor[0]+3>19)
                            coll=1;
                        else if(-j+cursor[0]+3>-1){
                            if(grid[-j+cursor[0]+3][+i+cursor[1]+2])
                                coll=1;
                        }    
                    }    
                    break;
                    default:
                    break;
                }    
            }
        }
    }
    return coll;
}
int isColliding(int piece[2][4],int angle,int cursor[2],int grid[20][10],int type){
    int coll=0;
    for(int i=0;i<2&&!coll;i++){
        for(int j=0;j<4&&!coll;j++){
            if (piece[i][j]){
                switch(angle){
                    case 0:
                    if(+j+cursor[1]>9||+j+cursor[1]<0)
                        coll=1;
                    else if(i+cursor[0]>19)
                            coll=1;
                    else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                            coll=1;
                    break;
                    case 1:
                    if(type==1||type==2||type==6){
                        if(+i+cursor[1]+2<0||+i+cursor[1]+2>9)
                            coll=1;
                        else if(-j+cursor[0]+2>19)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&grid[-j+cursor[0]+2][+i+cursor[1]+2])
                                coll=1;
                    }
                    else if(type==4){
                        if(+j+cursor[1]<0||+j+cursor[1]>9)
                            coll=1;
                        else if(i+cursor[0]>19)
                            coll=1;
                        else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                                coll=1;
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+2<0||+i+cursor[1]+2>9)
                            coll=1;
                        else if(-j+cursor[0]+1>19)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&grid[-j+cursor[0]+1][+i+cursor[1]+2])
                                coll=1;
                    }    
                    else{
                        if(+i+cursor[1]+2<0||+i+cursor[1]+2>9)
                            coll=1;
                        else if(-j+cursor[0]+2>19)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&grid[-j+cursor[0]+2][+i+cursor[1]+2])
                                coll=1;
                    }
                    break;
                    case 2:
                    if(type==1||type==2||type==6){
                        if(-j+cursor[1]+4<0||-j+cursor[1]+4>9)
                            coll=1;
                        else if(-i+cursor[0]>19)
                            coll=1;
                        else if(-i+cursor[0]>-1&&grid[-i+cursor[0]][-j+cursor[1]+4])
                                coll=1;
                    }
                    else if(type==4){
                        if(+j+cursor[1]<0||+j+cursor[1]>9)
                            coll=1;
                        else if(i+cursor[0]>19)
                            coll=1;
                        else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                                coll=1;
                    }    
                    else if(type==7){
                        if(+j+cursor[1]<0||+j+cursor[1]>9)
                            coll=1;
                        else if(i+cursor[0]>19)
                            coll=1;
                        else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                                coll=1;
                    }    
                    else{
                        if(+j+cursor[1]<0||+j+cursor[1]>9)
                            coll=1;
                        else if(i+cursor[0]>19)
                            coll=1;
                        else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                                coll=1;
                    }    
                    break;
                    case 3:
                    if(type==1||type==2||type==6){
                        if(-i+cursor[1]+2<0||-i+cursor[1]+2>9)
                            coll=1;
                        else if(j+cursor[0]-2>19)
                            coll=1;
                        else if(j+cursor[0]-2>-1&&grid[+j+cursor[0]-2][-i+cursor[1]+2])
                                coll=1;
                    }
                    else if(type==4){
                        if(+j+cursor[1]<0||+j+cursor[1]>9)
                            coll=1;
                        else if(i+cursor[0]>19)
                            coll=1;
                        else if(i+cursor[0]>-1&&grid[+i+cursor[0]][+j+cursor[1]])
                                coll=1;
                    }    
                    else if(type==7){
                        if(+i+cursor[1]+2<0||+i+cursor[1]+2>9)
                            coll=1;
                        else if(-j+cursor[0]+1>19)
                            coll=1;
                        else if(-j+cursor[0]+1>-1&&grid[-j+cursor[0]+1][+i+cursor[1]+2])
                                coll=1;
                    }    
                    else{
                        if(+i+cursor[1]+2<0||+i+cursor[1]+2>9)
                            coll=1;
                        else if(-j+cursor[0]+2>19)
                            coll=1;
                        else if(-j+cursor[0]+2>-1&&grid[-j+cursor[0]+2][+i+cursor[1]+2])
                                coll=1;
                    }    
                    break;
                    default:
                    break;
                }    
            }
        }
    }
    return coll;
}
