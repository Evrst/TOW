#include <SDL2/SDL_ttf.h>

int printScore(int score,SDL_Renderer* renderer,SDL_Texture* SCORE,SDL_Rect SCORERect,SDL_Texture* scoreText,SDL_Surface* *scoreSurface,SDL_Rect scoreRect,TTF_Font *textFont,SDL_Color blanc){

    SDL_RenderCopy(renderer,SCORE, NULL, &SCORERect);
    scoreRect.x = 310;
    scoreRect.y = 75;
    char scoreString[13];
    sprintf(scoreString,"%d",score);

    *scoreSurface = TTF_RenderText_Solid(textFont,scoreString,blanc);
    scoreText = SDL_CreateTextureFromSurface(renderer,*scoreSurface);
    SDL_QueryTexture(scoreText, NULL, NULL, &scoreRect.w, &scoreRect.h);
    SDL_RenderCopy(renderer,scoreText, NULL, &scoreRect);
}

int printLevel(int level,SDL_Renderer* renderer,SDL_Texture* LEVEL,SDL_Rect LEVELRect,SDL_Texture* levelText,SDL_Surface* *levelSurface,SDL_Rect levelRect,TTF_Font *textFont,SDL_Color blanc){

    SDL_RenderCopy(renderer,LEVEL, NULL, &LEVELRect);
    levelRect.x = 310;
    levelRect.y = 175;
    char levelString[5];
    sprintf(levelString,"%d",level);

    *levelSurface = TTF_RenderText_Solid(textFont,levelString,blanc);
    levelText = SDL_CreateTextureFromSurface(renderer,*levelSurface);
    SDL_QueryTexture(levelText, NULL, NULL, &levelRect.w, &levelRect.h);
    SDL_RenderCopy(renderer,levelText, NULL, &levelRect);
}
