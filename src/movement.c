#include "func.h"
#include <SDL2/SDL.h>
#include <time.h>
#include "config.h"


int shift(int cursor[2],int piece[2][4],int type,int angle
        ,int grid[20][10],int currentFrame,float *gravity,int gravityValue
        ,int *movingState,int *score){
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    static int lastFrame=-4;
    static int lastHardDrop=-4;
    static int hardDropStart=0;
    if(state[SDL_SCANCODE_LEFT]&&!state[SDL_SCANCODE_RIGHT]){
        if(*movingState>=0){
            lastFrame=currentFrame;
            *movingState=-1;
            if(!leftColliding(piece,angle,cursor,grid,type))
                cursor[1]--;
        }    
        if(*movingState==-1
                &&(lastFrame-currentFrame)*(lastFrame-currentFrame)>=256)
            *movingState=-2;
        if(*movingState==-2
                &&(lastFrame-currentFrame)*(lastFrame-currentFrame)>=4){
            lastFrame=currentFrame;
            if(!leftColliding(piece,angle,cursor,grid,type))
                cursor[1]--;
        }
    }   
    else if(state[SDL_SCANCODE_RIGHT]&&!state[SDL_SCANCODE_LEFT]){
        if(*movingState<=0){
            lastFrame=currentFrame;
            *movingState=1;
            if(!rightColliding(piece,angle,cursor,grid,type))
                cursor[1]++;
        }
        if(*movingState==1
                &&(lastFrame-currentFrame)*(lastFrame-currentFrame)>=256)
            *movingState=2;
        if(*movingState==2
                &&(lastFrame-currentFrame)*(lastFrame-currentFrame)>=4){
            lastFrame=currentFrame;
            if(!rightColliding(piece,angle,cursor,grid,type))
                cursor[1]++;
        }    
    }  
    else{
        *movingState=0;
    }    
    if(state[SDL_SCANCODE_DOWN]){
        if (*gravity!=0.5)
            hardDropStart=cursor[0];
        *gravity=0.5;
        if(downColliding(piece,angle,cursor,grid,type))
            if (lastHardDrop!=currentFrame){
                *score+=(cursor[0]-hardDropStart+2)/2+1;
                lastHardDrop=currentFrame;
            }
    }    
    else
        *gravity=gravityValue;
}
int rotate(int piece[2][4],int type,int *angle,int grid[20][10],int cursor[2]){
    static int lastState=0;
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    if((state[SDL_SCANCODE_RCTRL]||state[SDL_SCANCODE_SPACE])&&lastState>=0){
        lastState=-1;
        if(!isColliding(piece,*angle<3?(*angle+1):0,cursor,grid,type))
            *angle=*angle<3?(*angle+1):0;
    }
    if(state[SDL_SCANCODE_RSHIFT]&&lastState<=0){
        lastState=1;
        if(!isColliding(piece,*angle<3?(*angle+1):0,cursor,grid,type))
            *angle=*angle>0?(*angle-1):3;
    }
    if(!state[SDL_SCANCODE_RCTRL]&&!state[SDL_SCANCODE_SPACE]&&!state[SDL_SCANCODE_RSHIFT])
        lastState=0;
}        
